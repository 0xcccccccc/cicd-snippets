# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.1.0](https://gitlab.com/0xcccccccc/cicd-snippets/compare/v1.0.0...v1.1.0) (2021-07-06)


### Features

* add new snippets (main, flow, release) ([92783fa](https://gitlab.com/0xcccccccc/cicd-snippets/commit/92783fa324560b2bf28b01f1b913a12154bb8fb4))


### Bug Fixes

* **flow:** fixed wrong evaluation of release branch ([5f8b960](https://gitlab.com/0xcccccccc/cicd-snippets/commit/5f8b960ecd497f18c5e3ec6017863c16a1ba4db0))
