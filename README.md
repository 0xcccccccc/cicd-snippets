# CICD - Snippets

This repo contains several snippets that can be used in CI pipelines.

<!-- @import "[TOC]" {cmd="toc" depthFrom=1 depthTo=6 orderedList=false} -->

<!-- code_chunk_output -->

- [CICD - Snippets](#cicd-snippets)
    - [Usage](#usage)
    - [Main Snippets](#main-snippets)
    - [Docker Snippets](#docker-snippets)
      - [.snippet_docker_build](#snippet_docker_build)
      - [.snippet_docker_scan_build](#snippet_docker_scan_build)
      - [.snippet_docker_image_promote](#snippet_docker_image_promote)
    - [Release management snippets](#release-management-snippets)
      - [.snippet_release_standard_version_release](#snippet_release_standard_version_release)
    - [CI/CD flow snippets](#cicd-flow-snippets)
      - [.snippet_flow_run_only_on_tags_on_changelog_commits](#snippet_flow_run_only_on_tags_on_changelog_commits)
      - [.snippet_flow_run_only_on_release_branch_on_non_changelog_commits](#snippet_flow_run_only_on_release_branch_on_non_changelog_commits)
      - [.snippet_flow_run_always_except_changelog_commit](#snippet_flow_run_always_except_changelog_commit)
    - [Misc snippets](#misc-snippets)
      - [.snippet_misc_variables_info](#snippet_misc_variables_info)

<!-- /code_chunk_output -->

### Usage

1. Include `main.gitlab-ci.yml` to your _ci/cd yaml_
2. Include other the snippet files you require.

e.g.

```yml
include:
  - project: "0xcccccccc/cicd-snippets"
    ref: v1.0.1
    file: "main.gitlab-ci.yml"
  - project: "0xcccccccc/cicd-snippets"
    ref: v1.0.1
    file: "docker.gitlab-ci.yml"
  - project: "0xcccccccc/cicd-snippets"
    ref: v1.0.1
    file: "release.gitlab-ci.yml"
  - project: "0xcccccccc/cicd-snippets"
    ref: v1.0.1
    file: "flow.gitlab-ci.yml"
```

### Main Snippets

Main file that has to be included before all other snippet files. It defines variables or snippets that are required by other snippets.

Snippet file: [./main.gitlab-ci.yml](main.gitlab-ci.yml)

Global snippet vars:

| Var name                               | purpose                                                                                             | default value                                                   |
| -------------------------------------- | --------------------------------------------------------------------------------------------------- | --------------------------------------------------------------- |
| `SNIPPET_MAIN_RELEASE_BRANCH`          | defines the main stable branch where releases are triggered                                         | `main`                                                          |
| `SNIPPET_MAIN_CICD_ALPINE_TOOLS_IMAGE` | the [CICD - Alpine Tools Image](https://gitlab.com/0xcccccccc/cicd-alpine-tools-image) docker image | `registry.gitlab.com/0xcccccccc/cicd-alpine-tools-image:v0.2.0` |

### Docker Snippets

Docker related snippets for e.g. building, scanning, releasing.

Snippet file: [./docker.gitlab-ci.yml](docker.gitlab-ci.yml)

Global snippet vars:

| Var name                                | purpose                                          | default value                                       |
| --------------------------------------- | ------------------------------------------------ | --------------------------------------------------- |
| `SNIPPET_DOCKER_DEV_IMAGE_TAG_SHA`      | the image tag to be used for dev builds          | `CI_REGISTRY_IMAGE`/dev:`CI_COMMIT_SHORT_SHA `      |
| `SNIPPET_DOCKER_DEV_IMAGE_TAG_LATEST`   | the image tag for images that are released       | `CI_REGISTRY_IMAGE`/dev:`I_COMMIT_REF_SLUG`-latest" |
| `SNIPPET_DOCKER_RELEASE_IMAGE_REGISTRY` | the target image registry for releasing an image | `CI_REGISTRY_IMAGE `                                |

#### .snippet_docker_build

Can be used to build a docker image with [kaniko](https://github.com/GoogleContainerTools/kaniko). The output image will pe available under `SNIPPET_DOCKER_DEV_IMAGE_TAG_SHA` or `SNIPPET_DOCKER_DEV_IMAGE_TAG_LATEST`

Configuration vars:

| Var name                    | purpose                           | requried | default value when empty    |
| --------------------------- | --------------------------------- | -------- | --------------------------- |
| `SNIPPET_DOCKER_DOCKERFILE` | the path to the docker file       | no       | `CI_PROJECT_DIR`/Dockerfile |
| `SNIPPET_DOCKER_CONTEXT`    | the path of the working directory | no       | `CI_PROJECT_DIR`            |

#### .snippet_docker_scan_build

Use this snippet to scan an image for vulnerabilities by using [](aquasec/trivy)

Configuration vars:

| Var name                       | purpose               | requried | default value when empty           |
| ------------------------------ | --------------------- | -------- | ---------------------------------- |
| `SNIPPET_DOCKER_IMAGE_TO_SCAN` | the docker image path | no       | `SNIPPET_DOCKER_DEV_IMAGE_TAG_SHA` |

#### .snippet_docker_image_promote

Use this snippet to re-tag an previously built image by using [https://github.com/containers/skopeo](skopeo).

> This snippet will also create an `latest` tag

Configuration vars:

| Var name                                | purpose                                                                                           | requried | default value when empty                |
| --------------------------------------- | ------------------------------------------------------------------------------------------------- | -------- | --------------------------------------- |
| `SNIPPET_DOCKER_SRC_REGISTRY_USER`      | user name for source registry                                                                     | no       | `CI_REGISTRY_USER`                      |
| `SNIPPET_DOCKER_SRC_REGISTRY_PASSWORD`  | password for source registry                                                                      | no       | `CI_REGISTRY_PASSWORD`                  |
| `SNIPPET_DOCKER_DEST_REGISTRY_USER`     | user for destination registry                                                                     | no       | `CI_REGISTRY_USER`                      |
| `SNIPPET_DOCKER_DEST_REGISTRY_PASSWORD` | password for destination registry                                                                 | no       | `CI_REGISTRY_PASSWORD`                  |
| `SNIPPET_DOCKER_SRC_IMAGE`              | the source image to be tagged                                                                     | no       | `SNIPPET_DOCKER_DEV_IMAGE_TAG_SHA`      |
| `SNIPPET_DOCKER_DEST_REGISTRY`          | the destination registry for the image (without tag)                                              | no       | `SNIPPET_DOCKER_RELEASE_IMAGE_REGISTRY` |
| `SNIPPET_DOCKER_DEST_TAG`               | the destination tag                                                                               | no       | `CI_COMMIT_TAG`                         |
| `SNIPPET_RELEASE_IS_CHANGELOG_COMMIT`   | out variable which is set to "true" for the pipline<br>which is triggered by the changelog commit | -        | -                                       |

### Release management snippets

#### .snippet_release_standard_version_release

Makes a release by using [standard-version](https://github.com/conventional-changelog/standard-version).
The snippet will do the following:

- write the `CHANGELOG.md` and push the change
- calculcate release version based on commit massages and tag current branch

When pushing changes for `CHANGELOG.md` the resulting pipeline (triggered by this change) can be identified by variable `SNIPPET_RELEASE_IS_CHANGELOG_COMMIT` which is set to `"true"`

> _Standard-Version_ requires [conventional commits](https://www.conventionalcommits.org/en/v1.0.0/) to work correctly.

Configuration vars:

| Var name                             | purpose                                                                                    | requried | default value when empty                   |
| ------------------------------------ | ------------------------------------------------------------------------------------------ | -------- | ------------------------------------------ |
| `SNIPPET_RELEASE_GIT_USER_EMAIL`     | email of the user that is used to perform git commands (_tagging, commiting, pushing_)     | no       | `GITLAB_USER_EMAIL` (builtin variable)     |
| `SNIPPET_RELEASE_GIT_USER_NAME`      | user name of the user that is used to perform git commands (_tagging, commiting, pushing_) | no       | `GITLAB_USER_NAME` (builtin variable)      |
| `SNIPPET_RELEASE_GIT_API_TOKEN`      | API token of the git user used for authenthication                                         | yes      |                                            |
| `SNIPPET_RELEASE_COMMIT_MSG_FORMAT`  | message format which will be used for pushing changelog changes                            | no       | `chore(release): {{currentTag}} [ci skip]` |
| `SNIPPET_RELEASE_BRANCH_FOR_RELEASE` | target branch for release                                                                  | no       | `SNIPPET_MAIN_RELEASE_BRANCH`              |

### CI/CD flow snippets

Here you can find sinppets for `job rules` which can be used to define when jobs are executed.

Snippet file: [./flow.gitlab-ci.yml](flow.gitlab-ci.yml)

Global snippet vars:

#### .snippet_flow_run_only_on_tags_on_changelog_commits

Use if a job should only run if the pipeline trigger was the creation of an `tag` via snippet `.snippet_release_standard_version_release`.

#### .snippet_flow_run_only_on_release_branch_on_non_changelog_commits

Use if a job should run only for commits (not tags) on release branch (defined via `SNIPPET_MAIN_RELEASE_BRANCH`) and is not a _changelog commit_ pushed by snippet `.snippet_release_standard_version_release`.

#### .snippet_flow_run_always_except_changelog_commit

Use if a job should run on always (commits on all brnaches and for tags) except for _changelog commit_ pushed by snippet `.snippet_release_standard_version_release`.

### Misc snippets

Snippets that are not related to any special topic but may usefull.

Snippet file: [./misc.gitlab-ci.yml](misc.gitlab-ci.yml)

#### .snippet_misc_variables_info

Prints out the following variables:

1. All environment variables
2. All environment variables starting with `CI_`
3. All environment variables starting with `SNIPPET_`
